package model

import (
	"gmfs/core/tools/rdm"
	"gopkg.in/mgo.v2/bson"
	"time"
)

/**
 * 文件信息
 */
type Info struct {
	Id          bson.ObjectId `bson:"_id"`
	Fid         string        //唯一ID
	Rdm         string        //随机数
	Nude        bool          //是否涉黄
	Online      bool          //是否上线 true 永久存储 false 定期清理
	FileType    int           //类型 0、图片文件 1、二进制文件
	FileSuffix  string        //上传文件的后缀
	ContentType string        //MIME
	UploadDate  time.Time     //上传时间
}

func NewInfo() *Info {
	i := new(Info)
	i.SetId(bson.NewObjectId())
	i.SetRdm(rdm.NewRdm())
	return i
}

func NewInfoByRes(res *Res) *Info {
	i := NewInfo()
	i.SetFid(res.GetFid())
	i.SetNude(res.GetNude())
	i.SetOnline(res.GetOnline())
	i.SetFileType(res.GetFileType())
	i.SetFileSuffix(res.GetFileSuffix())
	i.SetContentType(res.GetContentType())
	i.SetUploadDate(time.Now())
	return i
}

//id
func (this *Info) SetId(id bson.ObjectId) {
	this.Id = id
}
func (this *Info) GetId() bson.ObjectId {
	return this.Id
}
func (this *Info) GetIdToString() string {
	return this.Id.Hex()
}

//fid
func (this *Info) SetFid(fid string) {
	this.Fid = fid
}
func (this *Info) GetFid() string {
	return this.Fid
}

//rdm
func (this *Info) SetRdm(rdm string) {
	this.Rdm = rdm
}
func (this *Info) GetRdm() string {
	return this.Rdm
}

//nude
func (this *Info) SetNude(nude bool) {
	this.Nude = nude
}
func (this *Info) GetNude() bool {
	return this.Nude
}

//online
func (this *Info) SetOnline(online bool) {
	this.Online = online
}
func (this *Info) GetOnline() bool {
	return this.Online
}

//fileType
func (this *Info) SetFileType(fileType int) {
	this.FileType = fileType
}
func (this *Info) GetFileType() int {
	return this.FileType
}

//fileSuffix
func (this *Info) SetFileSuffix(fileSuffix string) {
	this.FileSuffix = fileSuffix
}
func (this *Info) GetFileSuffix() string {
	return this.FileSuffix
}

//contentType
func (this *Info) SetContentType(contentType string) {
	this.ContentType = contentType
}
func (this *Info) GetContentType() string {
	return this.ContentType
}

//uploadDate
func (this *Info) SetUploadDate(uploadDate time.Time) {
	this.UploadDate = uploadDate
}
func (this *Info) GetUploadDate() time.Time {
	return this.UploadDate
}

package main

import (
	"fmt"
	"giraffe/core/web/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
)

func main() {
	session, err := mgo.Dial("127.0.0.1")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)

	c := session.DB("giraffe").C("img_classify")
	err = c.Insert(&model.ImgClassify{"photo", "rotate90", "", 80, 80},
		&model.ImgClassify{"photo", "flipHorizontal", "grayscale", 100, 100})
	if err != nil {
		log.Fatal(err)
	}

	result := []model.ImgClassify{}
	err = c.Find(bson.M{"name": "photo"}).All(&result)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("result:", result)
}

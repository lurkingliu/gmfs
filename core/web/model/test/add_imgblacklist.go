package main

import (
	"fmt"
	"giraffe/core/web/model"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"time"
)

func main() {
	session, err := mgo.Dial("127.0.0.1")
	if err != nil {
		panic(err)
	}
	defer session.Close()

	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)

	c := session.DB("giraffe").C("img_blacklist")
	err = c.Insert(&model.ImgBlacklist{"5594f5f307986af11c000002", "3877029ef58d4836f9307d23daf661b3",
		173845, time.Now()})
	if err != nil {
		log.Fatal(err)
	}

	result := []model.ImgBlacklist{}
	err = c.Find(bson.M{"md5": "3877029ef58d4836f9307d23daf661b3", "size": 173845}).All(&result)
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("result:", result)
}

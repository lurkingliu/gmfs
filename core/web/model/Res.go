package model

import (
	"gmfs/core/tools/util"
	"gopkg.in/mgo.v2/bson"
	"time"
)

/**
 * 文件资源
 */
type Res struct {
	Id          bson.ObjectId `bson:"_id"`
	Fid         string        //唯一ID
	Md5         string        //md5值
	Size        int           //大小
	Refnum      int           //上传次数
	Nude        bool          //是否涉黄
	Online      bool          //是否上线 true 永久存储 false 定期清理
	FileType    int           //类型 0、图片文件 1、二进制文件
	FileSuffix  string        //上传文件的后缀
	ContentType string        //MIME
	UploadDate  time.Time     //上传时间
}

func NewRes(data []byte) *Res {
	r := new(Res)
	r.Md5 = util.ByteMD5(data)
	r.Size = len(data)
	r.Refnum = 0
	return r
}

//id
func (this *Res) SetId(id string) {
	this.Id = bson.ObjectIdHex(id)
}
func (this *Res) GetId() bson.ObjectId {
	return this.Id
}
func (this *Res) GetIdToString() string {
	return this.Id.Hex()
}

//fid
func (this *Res) SetFid(fid string) {
	this.Fid = fid
}
func (this *Res) GetFid() string {
	return this.Fid
}

//md5
func (this *Res) SetMd5(md5 string) {
	this.Md5 = md5
}
func (this *Res) GetMd5() string {
	return this.Md5
}

//size
func (this *Res) SetSize(size int) {
	this.Size = size
}
func (this *Res) GetSize() int {
	return this.Size
}

//refnum
func (this *Res) GetRefnum() int {
	return this.Refnum
}

//nude
func (this *Res) SetNude(nude bool) {
	this.Nude = nude
}
func (this *Res) GetNude() bool {
	return this.Nude
}

//online
func (this *Res) SetOnline(online bool) {
	this.Online = online
}
func (this *Res) GetOnline() bool {
	return this.Online
}

//fileType
func (this *Res) SetFileType(fileType int) {
	this.FileType = fileType
}
func (this *Res) GetFileType() int {
	return this.FileType
}

//fileSuffix
func (this *Res) SetFileSuffix(fileSuffix string) {
	this.FileSuffix = fileSuffix
}
func (this *Res) GetFileSuffix() string {
	return this.FileSuffix
}

//contentType
func (this *Res) SetContentType(contentType string) {
	this.ContentType = contentType
}
func (this *Res) GetContentType() string {
	return this.ContentType
}

//uploadDate
func (this *Res) SetUploadDate(uploadDate time.Time) {
	this.UploadDate = uploadDate
}
func (this *Res) GetUploadDate() time.Time {
	return this.UploadDate
}

package model

import (
	"time"
)

/**
 * 文件黑名单
 */
type Blacklist struct {
	Fid        string    //唯一ID
	Md5        string    //md5值
	Size       int       //大小
	VerifyDate time.Time //认证时间
}

//fid
func (this *Blacklist) SetFid(fileId string) {
	this.Fid = fileId
}
func (this *Blacklist) GetFid() string {
	return this.Fid
}

//md5
func (this *Blacklist) SetMd5(md5 string) {
	this.Md5 = md5
}
func (this *Blacklist) GetMd5() string {
	return this.Md5
}

//size
func (this *Blacklist) SetSize(size int) {
	this.Size = size
}
func (this *Blacklist) GetSize() int {
	return this.Size
}

//verifyDate
func (this *Blacklist) SetVerifyDate(verifyDate time.Time) {
	this.VerifyDate = verifyDate
}
func (this *Blacklist) GetVerifyDate() time.Time {
	return this.VerifyDate
}

package handler

import (
	"gmfs/core/web/ink"
)

type jsonContext struct {
	context *ink.Context
	data    map[string]interface{}
}

// Json creates a json context response.
func Json(context *ink.Context, code string) *jsonContext {
	c := new(jsonContext)
	c.context = context
	c.data = make(map[string]interface{})
	c.data["code"] = code
	return c
}

func (jc *jsonContext) Set(key string, v interface{}) *jsonContext {
	jc.data[key] = v
	return jc
}

func (jc *jsonContext) End() {
	jc.context.Json(jc.data)
}

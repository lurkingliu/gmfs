package main

import (
	"fmt"
	"gopic/cache/redis"
)

func main() {
	var cache = redis.NewCache(60)
	var key = "hello_go"
	err := cache.Set(key, "hello redis go.")
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("cache set success.")
	}
	value, err := cache.Get(key)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(value)
}

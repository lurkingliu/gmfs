package storage

import (
	"log"
	"time"

	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// MgoStorage objects store and retrieve data using Mongo.
type MgoStorage struct {
	// mongo collection where the cache will be stored
	Collection *mgo.Collection
}

// New returns a new MgoStorage
func NewMgoStorage(collection *mgo.Collection) *MgoStorage {
	return &MgoStorage{
		Collection: collection,
	}
}

func (self *MgoStorage) Get(key string) (resp []byte, ok bool) {
	result := record{}
	err := self.Collection.Find(bson.M{"key": key}).One(&result)
	if err != nil {
		return []byte{}, false
	}

	return result.Content, true
}

func (self *MgoStorage) Set(key string, content []byte) {
	_, err := self.Collection.Upsert(bson.M{"key": key}, &record{
		Created: time.Now(),
		Updated: time.Now(),
		Key:     key,
		Content: content,
	})

	if err != nil {
		log.Printf("Can't insert record in mongo: %v\n", err)
		return
	}

	return
}

func (self *MgoStorage) Delete(key string) {
	err := self.Collection.Remove(bson.M{"key": key})
	if err != nil {
		log.Printf("Can't remove record: %s", err)
	}
}

func (self *MgoStorage) Indexes() {
	index := mgo.Index{
		Key:      []string{"key"},
		Unique:   true,
		DropDups: true,
	}

	err := self.Collection.EnsureIndex(index)
	if err != nil {
		log.Printf("Can't ensure index: %s", err)
	}
}

type record struct {
	Created time.Time
	Updated time.Time
	Key     string
	Content []byte
}
